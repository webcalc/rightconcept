from __future__ import absolute_import

import os
import shutil
import subprocess
import uuid
import youtube_dl
from .util import get_video_url
import logging
log = logging.getLogger(__name__)

def save_to_disk(url, path_dir):
    log.debug("Info extraction starting")
    info = get_video_url(url,path_dir)
    log.debug("Info extraction finished")
    result = None
    if info:
        youtube_id = info['id']
        title = info['title']
        log.debug('Video title={0}, video id = {1}'.format(title, youtube_id))
        video_filename = title+'.'+info['ext']
        result = {
            'youtube_id': youtube_id,
            'title': title
        }
        output_filepath = os.path.join(path_dir, video_filename)
        log.debug('output file path set to {}'.format(output_filepath))
        if os.path.exists(output_filepath):
            result['filename'] = video_filename
        else:
            download_result = download(url,video_filename,path_dir)
            # if extraction result is 0, extraction is successful
            if download_result == 0:
                result['filename']=video_filename

    return result


def download(url, video_filename, path_dir):
    # We're creating a temporary file in case multiple workers are in the
    # process of downloading the same video.
    log.debug("Getting ready for video download")
    temp_filepath = os.path.join(path_dir,
                                 '{0}_{1}'.format(uuid.uuid4(), video_filename))
    log.debug('Temporary file path set to {}'.format(temp_filepath))
    output_filepath = os.path.join(path_dir, video_filename)
    log.debug('Output file path set to {}'.format(output_filepath))
    log.debug("Calling subprocess")
    options = {
    'format': 'best',
    'outtmpl':unicode(temp_filepath)
    }
    with youtube_dl.YoutubeDL(options) as ydl:
        result = ydl.download([str(url)])
    log.debug("Got result to be {}".format(result))
    # Status code 0 is successful.
    if result == 0:
        # Move the temporary file to the proper location.
        log.debug("moving the temporary file to the proper location")
        shutil.move(temp_filepath, output_filepath)
        log.debug("File moved!")
    return result

