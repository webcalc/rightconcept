from __future__ import unicode_literals
import youtube_dl

__author__ = 'ephraim'

def get_video_url(url,path_dir):
    options = {
    'outtmpl': '%(title)s-%(id)s.%(ext)s',
    'format': 'best',
    'download_archive':path_dir
    }
    with youtube_dl.YoutubeDL(options) as ydl:
        r = ydl.extract_info(str(url), download=False)
        return r

