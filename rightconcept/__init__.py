# -*- coding: utf-8 -*-

"""
Created on 2016-07-08
:author: ephraim (splendidzigy24@gmail.com)
"""

from kotti.resources import File, Image
from pyramid.i18n import TranslationStringFactory
from kotti.views.slots import assign_slot

_ = TranslationStringFactory('rightconcept')


def kotti_configure(settings):
    """ Add a line like this to you .ini file::

            kotti.configurators =
                rightconcept.kotti_configure

        to enable the ``rightconcept`` add-on.

    :param settings: Kotti configuration dictionary.
    :type settings: dict
    """

    settings['pyramid.includes'] += ' rightconcept'
    settings['kotti.alembic_dirs'] += ' rightconcept:alembic'
    settings['kotti.available_types'] += (' rightconcept.resources.Videos'
                                          +' rightconcept.resources.Dropdown'
                                          +' rightconcept.resources.Video'
                                          +' rightconcept.resources.Slider'
                                          +' rightconcept.resources.SliderImage'
                                          +' rightconcept.resources.Blog'
                                          +' rightconcept.resources.BlogEntry'
                                          +' rightconcept.resources.WeekVideo'
                                          +' rightconcept.resources.Advert'
                                          +' rightconcept.resources.Gallery')
    settings['kotti.fanstatic.view_needed'] += ' rightconcept.fanstatic.css_and_js'
    Image.type_info.addable_to.append('Video')
    Image.type_info.addable_to.append('BlogEntry')
    Image.type_info.addable_to.append('Videos')
    Image.type_info.addable_to.append('Blog')
    Image.type_info.addable_to.append('Gallery')




def includeme(config):
    """ Don't add this to your ``pyramid_includes``, but add the
    ``kotti_configure`` above to your ``kotti.configurators`` instead.

    :param config: Pyramid configurator object.
    :type config: :class:`pyramid.config.Configurator`
    """

    assign_slot('recent_videos','right')
    config.add_translation_dirs('rightconcept:locale')
    config.add_static_view('static-rightconcept', 'rightconcept:static')
    config.scan(__name__)
