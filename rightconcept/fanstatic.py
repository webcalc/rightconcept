# -*- coding: utf-8 -*-

"""
Created on 2016-07-08
:author: ephraim (splendidzigy24@gmail.com)
"""

from __future__ import absolute_import

from fanstatic import Group
from fanstatic import Library
from fanstatic import Resource
from js.bootstrap import bootstrap_css, bootstrap_js
from kotti.fanstatic import view_needed, edit_needed


library = Library("rightconcept", "static")


css = Resource(
    library,
    "styles.css",
    minified="styles.min.css",
    depends=[bootstrap_css],
    )
js = Resource(
    library,
    "scripts.js",
    minified="scripts.min.js",
    depends=[bootstrap_js],
    bottom=True)

fancybox_css = Resource(
    library,
    "fancybox/jquery.fancybox.css",
   depends=[bootstrap_css],
)
wheel_js = Resource(
    library,
    "fancybox/jquery.mousewheel-3.0.6.pack.js",
    depends=[bootstrap_js]
)
fancybox_js = Resource(
    library,
    "fancybox/jquery.fancybox.pack.js",
    depends=[bootstrap_js],
)
css_and_js = Group([css, js])
view_needed.add(css_and_js)
edit_needed.add(css_and_js)

fancybox = Group([wheel_js,fancybox_js, fancybox_css])