# -*- coding: utf-8 -*-

"""
Created on 2016-07-08
:author: ephraim (splendidzigy24@gmail.com)
"""

from kotti.interfaces import IDefaultWorkflow
from kotti.resources import Content, Image,Document
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Unicode, UnicodeText
from sqlalchemy import DateTime
from zope.interface import implements
from rightconcept import _
import datetime
from webhelpers.date import time_ago_in_words
from webhelpers.html import literal
from webhelpers.text import truncate
from webhelpers.html.tools import strip_tags



class Dropdown(Content):
    implements(IDefaultWorkflow)

    id = Column(Integer, ForeignKey('contents.id'), primary_key=True)
    type_info = Content.type_info.copy(
        name=u'Dropdown',
        title=_(u'Dropdown'),
        add_view=u'add_dropdown_page',
        addable_to=[u'Document']
    )

class Gallery(Content):
    implements(IDefaultWorkflow)

    id = Column(Integer, ForeignKey('contents.id'), primary_key=True)
    type_info = Content.type_info.copy(
        name=u'Gallery',
        title=_(u'Gallery'),
        add_view=u'add_gallery',
        addable_to=[u'Document',u'Dropdown']
    )
class Slider(Content):
    implements(IDefaultWorkflow)

    id = Column(Integer, ForeignKey('contents.id'), primary_key=True)
    type_info = Content.type_info.copy(
        name=u'Slider',
        title=_(u'Slider'),
        add_view=u'add_slider',
        addable_to=[u'Document']
    )

class SliderImage(Image):
    id = Column(Integer, ForeignKey("images.id"),primary_key=True)
    link = Column(Unicode(80))
    type_info = Image.type_info.copy(
        name=u"SliderImage",
        title=_(u"Slider Image"),
        add_view = u'add_slider_image',
        addable_to = [u'Slider',u'Advert'],
        selectable_default_views=[],
        uploadable_mimetypes=['image/*', ],
    )

class Advert(Content):
    id = Column(Integer, ForeignKey("contents.id"),primary_key=True)
    type_info = Content.type_info.copy(
        name=u"Advert",
        title=_(u"Advert"),
        add_view = u'add_advert',
        addable_to = [u'Document']
    )

class Videos(Document):
    id = Column(Integer, ForeignKey('documents.id'), primary_key=True)
    type_info = Document.type_info.copy(
        name=u'Videos',
        title=_(u'Videos'),
        add_view=u'add_videos',
        addable_to=[u'Document',u'Dropdown']
    )


class Video(Document):

    id = Column(Integer, ForeignKey('documents.id'), primary_key=True)
    video_url = Column(Unicode(100))
    views = Column(Integer)
    likes = Column(Integer)
    type_info = Document.type_info.copy(
        name=u'Video',
        title=_(u'Video'),
        add_view=u'add_video',
        addable_to=[u'Videos']
    )

    def __init__(self, video_url=None,views=0,likes = 0, **kwargs):
        super(Video,self).__init__(**kwargs)
        self.video_url = video_url
        self.views = views
        self.likes = likes


    @property
    def timestamp(self):
        today = datetime.date.today()
        yesterday = today-datetime.timedelta(days=1)
        if self.creation_date.date() == today:
            return time_ago_in_words(self.creation_date,granularity="minute")+" ago"
        elif self.creation_date.date()==yesterday:
            return time_ago_in_words(self.creation_date,granularity="day")+" ago"
        else:
            return self.creation_date.strftime("%a %b %d, %Y")

class WeekVideo(Content):
    id = Column(Integer, ForeignKey('contents.id'), primary_key=True)
    video_url = Column(Unicode(100))
    type_info = Content.type_info.copy(
        name=u'WeekVideo',
        title = _(u'Week Video'),
        add_view = u'add_week_video',
        addable_to = [u'Document']
    )
class Blog(Document):
    id = Column(Integer, ForeignKey('documents.id'), primary_key=True)
    type_info = Document.type_info.copy(
        name=u'Blog',
        title=_(u'Blog'),
        add_view=u'add_blog',
        addable_to=[u'Document',u'Dropdown']
    )


class BlogEntry(Document):

    id = Column(Integer, ForeignKey('documents.id'), primary_key=True)
    type_info = Document.type_info.copy(
        name=u'BlogEntry',
        title=_(u'BlogEntry'),
        add_view=u'add_Blog_Entry',
        addable_to=[u'Blog']
    )

    @property
    def timestamp(self):
        today = datetime.date.today()
        yesterday = today-datetime.timedelta(days=1)
        if self.creation_date.date() == today:
            return time_ago_in_words(self.creation_date,granularity="minute")+" ago"
        elif self.creation_date.date()==yesterday:
            return time_ago_in_words(self.creation_date,granularity="day")+" ago"
        else:
            return self.creation_date.strftime("%a %b %d, %Y")
    @property
    def excerpt(self):
        result= truncate(strip_tags(literal(self.body)),length=300, whole_word=True)

        return result