# -*- coding: utf-8 -*-

"""
Created on 2016-07-08
:author: ephraim (splendidzigy24@gmail.com)
"""
from __future__ import unicode_literals
import random, os
from kotti import DBSession
from kotti.resources import get_root
from kotti.security import has_permission
from pyramid.view import view_config
from pyramid.view import view_defaults
from pyramid.response import Response, FileResponse
from webhelpers.paginate import PageURL_WebOb, Page
from rightconcept import _
from rightconcept.fanstatic import fancybox
from rightconcept.resources import Slider, Videos,Video, Blog, BlogEntry, WeekVideo, Advert, Gallery
from rightconcept.views import BaseView
from ..tasks import save_to_disk
from  sqlalchemy.sql.expression import func

@view_defaults(context=Videos, permission='view')
class VideoPageViews(BaseView):
    """ Views for :class:`rightconcept.resources.Videos` """

    @view_config(name='view', permission='view',
                 renderer='rightconcept:templates/view_videopage.pt')
    def view_videos(self):
        """ Default view for :class:`rightconcept.resources.Videos`

        :result: Dictionary needed to render the template.
        :rtype: dict
        """
        query = DBSession.query(Video)
        query = query.filter(Video.parent_id == self.context.id)
        query = query.order_by(func.random())
        items = query.all()
        items = [item for item in items if has_permission('view', item, self.request)]
        page = int(self.request.params.get('page', 1))
        page_url = PageURL_WebOb(self.request)
        paginator=Page(items, page, url=page_url,
                    items_per_page=9)


        return dict(paginator=paginator)


    @view_config(context=Video, permission='view',
                 renderer='rightconcept:templates/view_video.pt')
    def view(self):

        self.context.views+=1
        items = self.context.parent.children
        items = [item for item in items if has_permission('view', item, self.request) and not item.id==self.context.id]
        lower_Ad = DBSession.query(Advert).filter(Advert.title==u'LowerAd').first()
        z = None
        if lower_Ad:
            lower_Ad = random.choice(lower_Ad.children_with_permission(self.request))
            z=1
        return dict(items=items,z=z,lower_ad=lower_Ad)

@view_defaults(context=Blog, permission='view')
class JokePageViews(BaseView):
    """ Views for :class:`rightconcept.resources.Blog` """

    @view_config(name='view', permission='view',
                 renderer='rightconcept:templates/view_jokepage.pt')
    def view_jokes(self):
        """ Default view for :class:`rightconcept.resources.Videos`

        :result: Dictionary needed to render the template.
        :rtype: dict
        """
        query = DBSession.query(BlogEntry)
        query = query.filter(BlogEntry.parent_id == self.context.id)
        query = query.order_by(BlogEntry.creation_date.desc())
        items = query.all()
        items = [item for item in items if has_permission('view', item, self.request)]
        page = int(self.request.params.get('page', 1))
        page_url = PageURL_WebOb(self.request)
        paginator=Page(items, page, url=page_url,
                    items_per_page=10)

        return dict(paginator=paginator)


    @view_config(context=BlogEntry, permission='view',
                 renderer='rightconcept:templates/view_joke.pt')
    def view(self):
        return dict()

@view_defaults(context=Gallery, permission='view')
class Gery(BaseView):
    """ Views for :class:`rightconcept.resources.Gallery` """

    @view_config(name='view', permission='view',
                 renderer='rightconcept:templates/gallery.pt')
    def view(self):
        items = self.context.children
        items = [item for item in items if has_permission('view', item, self.request)]
        page = int(self.request.params.get('page', 1))
        page_url = PageURL_WebOb(self.request)
        paginator=Page(items, page, url=page_url,
                    items_per_page=24)

        fancybox.need()
        return dict(paginator=paginator)


@view_config(name='front-page',
        renderer='rightconcept:templates/front-page.pt',permission='view')
def front_page(request):
    query = DBSession.query(Video)
    query = query.order_by(func.random())
    items = query.all()
    items = [item for item in items if has_permission('view', item, request)]
    page = int(request.params.get('page', 1))
    page_url = PageURL_WebOb(request)
    paginator=Page(items, page, url=page_url,
                    items_per_page=9)
    pictures = DBSession.query(Slider).filter(Slider.title==u'Home Slider').first()
    if pictures:
        pictures = pictures.children_with_permission(request)
    else:
        pictures = None
    lower_Ad = DBSession.query(Advert).filter(Advert.title==u'LowerAd').first()
    if lower_Ad and lower_Ad.children:
        lower_Ad = random.choice(lower_Ad.children_with_permission(request))
    else:
        lower_Ad =None
    gallery = DBSession.query(Gallery).first()
    if gallery:
        pix = gallery.children_with_permission(request)
    else:
        pix = None
    return dict(paginator=paginator, pictures=pictures,lower_ad=lower_Ad, pix=pix)



@view_config(name='recent_videos', renderer='rightconcept:templates/recent_videos.pt')
def r_vid(request):
    query = DBSession.query(Video)
    query = query.order_by(Video.creation_date.desc())
    items = query.limit(5).all()
    items = [item for item in items if has_permission('view', item, request)]
    weekV = DBSession.query(WeekVideo).first()
    res = None
    if weekV is not None:
        res=1
    topAd = DBSession.query(Advert).filter(Advert.title==u'TopAd').first()
    if topAd and topAd.children:
        topAd = random.choice(topAd.children_with_permission(request))

    else:
        topAd=None

    return dict(items=items,weekV=weekV, res=res, topAd=topAd)




@view_config(name='download_video')
def dl(request):
    url = request.params.get('url')
    path_dir = request.registry.settings['app.download_path']
    result= save_to_disk(url, path_dir)
    if result:
        filename = result['filename']
        filepath = os.path.join(path_dir,filename)
        file_exists = os.path.exists(filepath)
        if file_exists:
            res = FileResponse(filepath,content_type=str('video/mp4'))
            res.headers[str('Content-Disposition')] = str('attachment;filename=%s'%filename)
            return res
        else:
            return Response("Unable to find {}.".format(request.path_info))

    return Response("Unable to download video. please try again")

def populate():

    site = get_root()
    site.default_view = 'front-page'
