# -*- coding: utf-8 -*-

"""
Created on 2016-07-08
:author: ephraim (splendidzigy24@gmail.com)
"""

import colander, deform
import random
from StringIO import StringIO
from kotti.views.edit import ContentSchema, DocumentSchema
from kotti.views.form import AddFormView, validate_file_size_limit, get_appstruct
from kotti.views.form import EditFormView
from kotti.views.edit.content import FileUploadTempStore,\
    FileEditForm, FileAddForm
from pyramid.view import view_config
from rightconcept import _
from rightconcept.resources import Video, Videos, SliderImage, Slider, Blog, BlogEntry, Dropdown,WeekVideo, Advert, Gallery
from kotti.util import _to_fieldstorage
from rightconcept.util import get_video_url


def FileSchema(tmpstore, title_missing=None):
    class FileSchema(ContentSchema):
        link = colander.SchemaNode(
            colander.String(),
            title = _(u"Link"),
            missing = colander.null,
        )
        file = colander.SchemaNode(
            deform.FileData(),
            title=_(u'Image'),
            widget=deform.widget.FileUploadWidget(tmpstore),
            validator=validate_file_size_limit,
            )
    def set_title_missing(node, kw):
        if title_missing is not None:
            node['title'].missing = title_missing

    return FileSchema(after_bind=set_title_missing)

class WeekVideoSchema(ContentSchema):

    video_url = colander.SchemaNode(colander.String(), title=_(u'Video Link'))

class VideoSchema(DocumentSchema):

    video_url = colander.SchemaNode(colander.String(), title=_(u'Video Link'))



class VideoPageSchema(DocumentSchema):
    title = colander.SchemaNode(
        colander.String(),
        title=_(u'Video Page Name')
    )

@view_config(name=Dropdown.type_info.add_view,
             permission=Dropdown.type_info.add_permission,
             renderer='kotti:templates/edit/node.pt')
class DropdownAddForm(AddFormView):
    """ Form to add a new instance of Video. """

    schema_factory = ContentSchema
    add = Dropdown
    item_type = _(u"Dropdown")


@view_config(name='edit', context=Dropdown, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class DropdownEditForm(EditFormView):
    """ Form to edit existing Video Page objects. """

    schema_factory = ContentSchema


@view_config(name=Gallery.type_info.add_view,
             permission=Gallery.type_info.add_permission,
             renderer='kotti:templates/edit/node.pt')
class GalleryAddForm(AddFormView):
    """ Form to add a new instance of Video. """

    schema_factory = ContentSchema
    add = Gallery
    item_type = _(u"Gallery")


@view_config(name='edit', context=Gallery, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class GalleryEditForm(EditFormView):
    """ Form to edit existing Video Page objects. """

    schema_factory = ContentSchema


@view_config(name=Videos.type_info.add_view,
             permission=Videos.type_info.add_permission,
             renderer='kotti:templates/edit/node.pt')
class VideoPageAddForm(AddFormView):
    """ Form to add a new instance of Video. """

    schema_factory = VideoPageSchema
    add = Videos
    item_type = _(u"Videos")


@view_config(name='edit', context=Videos, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class VideoPageEditForm(EditFormView):
    """ Form to edit existing Video Page objects. """

    schema_factory = VideoPageSchema


@view_config(name=Video.type_info.add_view,
             permission=Video.type_info.add_permission,
             renderer='kotti:templates/edit/node.pt')
class VideoAddForm(AddFormView):
    """ Form to add a new instance of Video. """

    item_type = _(u'Video')
    add = Video
    schema_factory = VideoSchema




@view_config(name='edit', context=Video, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class VideoEditForm(EditFormView):
    """ Form to edit existing Video objects. """
    schema_factory = VideoSchema


@view_config(name=WeekVideo.type_info.add_view,
             permission=WeekVideo.type_info.add_permission,
             renderer='kotti:templates/edit/node.pt')
class WeekVideoAddForm(AddFormView):
    """ Form to add a new instance of Video. """

    item_type = _(u'WeekVideo')
    add = WeekVideo
    schema_factory = WeekVideoSchema


@view_config(name='edit', context=WeekVideo, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class WeekVideoEditForm(EditFormView):
    """ Form to edit existing Video objects. """
    schema_factory = WeekVideoSchema

@view_config(name=Advert.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class AdvertAddform(AddFormView):
    schema_factory = ContentSchema
    add = Advert
    item_type = _(u'Advert')


@view_config(name='edit', context=Advert, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class AdvertEditForm(EditFormView):
    schema_factory = ContentSchema


@view_config(name=Slider.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class SliderAddform(AddFormView):
    schema_factory = ContentSchema
    add = Slider
    item_type = _(u'Slider')


@view_config(name='edit', context=Slider, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class SliderEditForm(EditFormView):
    schema_factory = ContentSchema

@view_config(name=SliderImage.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class SliderImageAddform(AddFormView):
    item_type = _(u'SliderImage')
    item_class = SliderImage

    def schema_factory(self):
        tmpstore = FileUploadTempStore(self.request)
        return FileSchema(tmpstore, title_missing=colander.null)

    def save_success(self, appstruct):
        if not appstruct['title']:
            appstruct['title'] = appstruct['file']['filename']
        return super(SliderImageAddform, self).save_success(appstruct)

    def add(self, **appstruct):
        filename = appstruct['file']['filename']
        item = self.item_class(
            title=appstruct['title'] or filename,
            description=appstruct['description'],
            link = appstruct['link'],
            tags=appstruct['tags'],
            data=_to_fieldstorage(**appstruct['file']),
        )
        return item


@view_config(name="edit",context=SliderImage,permission="edit",
             renderer = "kotti:templates/edit/node.pt")
class SliderImageEditForm(EditFormView):

    def before(self, form):
        form.appstruct = get_appstruct(self.context, self.schema)
        if self.context.data is not None:
            form.appstruct.update({'file': {
                'fp': StringIO(self.context.data.file.read()),
                'filename': self.context.name,
                'mimetype': self.context.mimetype,
                'uid': str(random.randint(1000000000, 9999999999)),
            }})

    def schema_factory(self):
        tmpstore = FileUploadTempStore(self.request)
        return FileSchema(tmpstore)

    def edit(self, **appstruct):
        title = appstruct['title']
        self.context.title = title
        self.context.description = appstruct['description']
        self.context.tags = appstruct['tags']
        self.context.link = appstruct['link']
        if appstruct['file']:
            self.context.data = _to_fieldstorage(**appstruct['file'])



@view_config(name=Blog.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class JokePageAddform(AddFormView):
    schema_factory = DocumentSchema
    add = Blog
    item_type = _(u'Blog')


@view_config(name='edit', context=Blog, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class JokePageEditForm(EditFormView):
    schema_factory = DocumentSchema


@view_config(name=BlogEntry.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class JokeAddform(AddFormView):
    schema_factory = DocumentSchema
    add = BlogEntry
    item_type = _(u'BlogEntry')


@view_config(name='edit', context=BlogEntry, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class JokeEditForm(EditFormView):
    schema_factory = DocumentSchema