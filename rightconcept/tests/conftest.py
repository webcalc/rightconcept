# -*- coding: utf-8 -*-

"""
Created on 2016-07-08
:author: ephraim (splendidzigy24@gmail.com)
"""

pytest_plugins = "kotti"

from pytest import fixture


@fixture(scope='session')
def custom_settings():
    import rightconcept.resources
    rightconcept.resources  # make pyflakes happy
    return {
        'kotti.configurators': 'kotti_tinymce.kotti_configure '
                               'rightconcept.kotti_configure'}
